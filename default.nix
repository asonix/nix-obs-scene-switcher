let
  pkgs = import <nixpkgs> { };
in
with pkgs; {
  obs-scene-switcher = qt6Packages.callPackage ./obs-scene-switcher.nix { };
}
