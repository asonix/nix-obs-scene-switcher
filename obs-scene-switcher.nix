{ lib
, stdenv
, fetchFromGitHub
, cmake
, curlFull
, pkg-config
, procps
, qtbase
, obs-studio
, opencv
, xorg
}:

stdenv.mkDerivation rec {
  pname = "obs-scene-switcher";
  version = "1.21.0";

  src = fetchFromGitHub {
    owner = "WarmUpTill";
    repo = "SceneSwitcher";
    rev = version;
    sha256 = "QVJZBH4rTz/RnSg1oIL9tP8y5g+i1/3Hu/WaOl+vSf4=";
    fetchSubmodules = true;
  };

  nativeBuildInputs = [ cmake pkg-config ];
  buildInputs = with xorg; [ curlFull libXScrnSaver libXtst qtbase opencv procps ];

  dontWrapQtApps = true;

  cmakeFlags = with lib; [
    "-DDEB_INSTALL=1"
    "-DCMAKE_BUILD_TYPE=Release"
    "-DLINUX_PORTABLE=ON"
    "-DBUILD_OUT_OF_TREE=1"
    "-Dlibobs_DIR=${obs-studio.out}/lib/cmake/libobs"
    "-Dobs-frontend-api_DIR=${obs-studio.out}/lib/cmake/obs-frontend-api"
  ];

  patches = [ ];

  meta = with lib; {
    description = "An automated scene switcher for OBS Studio ";
    homepage = "https://obsproject.com/forum/resources/automatic-scene-switching.395/";
    maintainers = [ ];
    license = licenses.gpl2Plus;
    platforms = [ "x86_64-linux" ];
  };
}
