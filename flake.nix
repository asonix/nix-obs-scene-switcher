{
  description = "obs-scene-switcher packaged";

  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-22.11;

  outputs = { nixpkgs, ... }:
    let
      pkgs = import nixpkgs {
        system = "x86_64-linux";
      };
    in
    {
      obs-scene-switcher = pkgs.qt6Packages.callPackage ./obs-scene-switcher.nix { };
    };
}
